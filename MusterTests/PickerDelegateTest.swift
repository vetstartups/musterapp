//
//  PickerDelegateTest.swift
//  Muster
//
//  Created by jhampac on 3/15/16.
//  Copyright © 2016 jhampac. All rights reserved.
//

import XCTest
@testable import Muster

class PickerDelegateTest: XCTestCase
{
    var testPickerDelegate: PickerDelegate!
    
    override func setUp()
    {
        super.setUp()
        let testDictionary = (key: "color", data: ["blue"])
        testPickerDelegate = PickerDelegate(info: testDictionary)
    }
    
    override func tearDown()
    {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testCreateDictionary()
    {
        let info = testPickerDelegate.createDictionary(0)
        
        XCTAssertEqual(info, ["color": "blue"], "This should create a dictionary using its own data internally")
    }
}