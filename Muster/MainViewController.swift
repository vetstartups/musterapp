//
//  MainViewController.swift
//  Muster
//
//  Created by jhampac on 3/11/16.
//  Copyright © 2016 jhampac. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import SwiftValidator
import Firebase

class MainViewController: UIViewController, UITextFieldDelegate, ValidationDelegate
{
    // MARK: - VC Properties
    
    let textFieldValidator = Validator()
    
    let militaryBranches = (key: "branch", data: ["Army", "USMC", "Navy", "Airforce", "USARNG"])
    var branchPickerSource: PickerDelegate!
    
    let militaryRanks = (key: "rank", data: ["E-1", "E-2", "E-3", "E-4", "E-5", "E-6", "E-7", "E-8", "E-9", "Officer"])
    var militaryRanksSource: PickerDelegate!
    
    let etsReason = (key: "status", data: ["Separating", "Retiring"])
    var etsReasonSource: PickerDelegate!
    
    var prospectInfo = [String: AnyObject]()
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var phoneTextField: VSTextField! { didSet { phoneTextField.formatting = .PhoneNumber }}
    @IBOutlet weak var firstNameTextField: VSTextField! { didSet { firstNameTextField.formatting = .NoFormatting }}
    @IBOutlet weak var lastNameTextField: VSTextField! { didSet { lastNameTextField.formatting = .NoFormatting }}
    @IBOutlet weak var mosTextField: VSTextField! { didSet { mosTextField.formatting = .NoFormatting}}
    
    @IBOutlet weak var branchUIPicker: UIPickerView!
    @IBOutlet weak var ranksUIPicker: UIPickerView!
    @IBOutlet weak var statusUIPicker: UIPickerView!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet var textFields: [UITextField]!
    @IBOutlet var uiPickers: [UIPickerView]!
    
    // MARK: - IBActions
    
    @IBAction func submitButtonTapped(sender: UIButton)
    {
        textFieldValidator.validate(self)
    }
    
    // MARK: - VC Life Cycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        // add blur effect over background image
        let blurEffect = UIBlurEffect(style: .Dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.view.bounds
        view.insertSubview(blurEffectView, atIndex: 1)
        
        // round out submit button
        submitButton.layer.cornerRadius = submitButton.frame.height / 2
        
        self.configureTextFields()
        self.configureUIPickers()
    }
    
    // MARK: - VC Methods
    
    func configureTextFields()
    {
        // tag each textfield with a key so we can create a dictionary for JSON
        firstNameTextField.key = "first_name"
        lastNameTextField.key = "last_name"
        phoneTextField.key = "mobile"
        mosTextField.key = "mos"
        
        // iterate through all the textfields
        for textfield in textFields
        {
            textfield.delegate = self
        }
        
        // the following registers each textField with a rule
        textFieldValidator.registerField(firstNameTextField, rules: [RequiredRule()])
        textFieldValidator.registerField(lastNameTextField, rules: [RequiredRule()])
        textFieldValidator.registerField(phoneTextField, rules: [RequiredRule(), PhoneNumberRule()])
        textFieldValidator.registerField(mosTextField, rules: [RequiredRule()])
    }
    
    func configureUIPickers()
    {
        branchPickerSource = PickerDelegate(info: militaryBranches)
        branchUIPicker.delegate = branchPickerSource
        branchUIPicker.dataSource = branchPickerSource
        branchUIPicker.backgroundColor = UIColor.clearColor()
        
        militaryRanksSource = PickerDelegate(info: militaryRanks)
        ranksUIPicker.delegate = militaryRanksSource
        ranksUIPicker.dataSource = militaryRanksSource
        ranksUIPicker.backgroundColor = UIColor.clearColor()
        
        etsReasonSource = PickerDelegate(info: etsReason)
        statusUIPicker.delegate = etsReasonSource
        statusUIPicker.dataSource = etsReasonSource
        statusUIPicker.backgroundColor = UIColor.clearColor()
        
        datePicker.setValue(UIColor.whiteColor(), forKey: "textColor")
        datePicker.datePickerMode = .Date
        datePicker.minimumDate = NSDate()
    }
    
    func showAlert(title: String, message: String)
    {
        let modal = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        let okAction = UIAlertAction(title: "Ok", style: .Cancel, handler: nil)
        modal.addAction(okAction)
        self.presentViewController(modal, animated: true, completion: nil)
    }
    
    func clearAll()
    {
        for textfield in self.textFields
        {
            textfield.text = ""
        }
        
        for uipicker in self.uiPickers
        {
            uipicker.selectRow(0, inComponent: 0, animated: true)
        }
        
        self.datePicker.date = NSDate()
        self.prospectInfo.removeAll(keepCapacity: true)
    }
    
    func saveProspectInfo(info: [String: AnyObject])
    {
        FireService.shared.save(info) { [unowned self] (error, firebase) -> Void in
            
            if error != nil
            {
                let title = "Error"
                let message = "Oops there was an error \(error.localizedDescription)"
                self.showAlert(title, message: message)
            }
            else
            {
                let title = "Success"
                let message = "Thank you, we will be contacting you real soon about info sessions and class dates"
                self.showAlert(title, message: message)
                self.clearAll()
                self.submitButton.enabled = true
                self.submitButton.setTitle("Submit", forState: .Normal)
                self.submitButton.backgroundColor = UIColor.redColor()
                self.activityIndicator.stopAnimating()
            }
        }
    }
    
    // MARK: - Validation Delegate Methods
    
    func validationSuccessful()
    {
        self.submitButton.enabled = false
        self.submitButton.setTitle("One Moment", forState: .Normal)
        self.submitButton.backgroundColor = UIColor.grayColor()
        activityIndicator.startAnimating()
        
        let first = firstNameTextField.createDictionary()
        let last = lastNameTextField.createDictionary()
        let branch = branchPickerSource.createDictionary(branchUIPicker.selectedRowInComponent(0))
        let mos = mosTextField.createDictionary()
        let rank = militaryRanksSource.createDictionary(ranksUIPicker.selectedRowInComponent(0))
        let ets = etsReasonSource.createDictionary(statusUIPicker.selectedRowInComponent(0))
        let number = phoneTextField.createDictionary()
        
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let date = dateFormatter.stringFromDate(datePicker.date)
        let dateDictionary = ["ets_date": date]
        
        prospectInfo.update(first)
        prospectInfo.update(last)
        prospectInfo.update(number)
        prospectInfo.update(branch)
        prospectInfo.update(mos)
        prospectInfo.update(rank)
        prospectInfo.update(ets)
        prospectInfo.update(dateDictionary)
        
        saveProspectInfo(prospectInfo)
    }
    
    func validationFailed(errors:[UITextField:ValidationError])
    {
        let title = ""
        let message = "Oops, please fill out all input fields"
        showAlert(title, message: message)
    }
    
    // MARK: - TextField Delegate
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
}