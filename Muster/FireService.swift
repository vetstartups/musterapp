//
//  FireService.swift
//  Muster
//
//  Created by jhampac on 4/19/16.
//  Copyright © 2016 jhampac. All rights reserved.
//

import Firebase
import Foundation

class FireService
{
    private let PROSPECT_REF = Firebase(url: "https://shining-heat-4651.firebaseio.com/prospects")
    
    static let shared = FireService()
    
    func save(info: [String: AnyObject], completionBlock: (error: NSError!, firebase: Firebase!) -> Void)
    {
        
        self.PROSPECT_REF.childByAutoId().setValue(info, withCompletionBlock: completionBlock)
    }
}