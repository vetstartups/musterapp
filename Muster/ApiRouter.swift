//
//  ApiRouter.swift
//  Muster
//
//  Created by jhampac on 3/20/16.
//  Copyright © 2016 jhampac. All rights reserved.
//

import Alamofire
import Foundation

enum ApiRouter: URLRequestConvertible
{
    static let baseURLPath = ""
    
    // Do a case on which endpoint
    case Register([String: AnyObject])
    
    // this is a computed property that gets used somewhere when Alamofire needs it
    var URLRequest: NSMutableURLRequest {
        
        // return a tuple from closure
        let result: (path: String, method: Alamofire.Method, parameters: [String: AnyObject]) = {
            
            switch self {
            case .Register(let info):
                return ("/register", .POST, info)
            }
        }()
        
        // use result tuple here
        let URL = NSURL(string: ApiRouter.baseURLPath)!
        let URLRequest = NSMutableURLRequest(URL: URL.URLByAppendingPathComponent(result.path))
        URLRequest.HTTPMethod = result.method.rawValue
        //URLRequest.setValue(ApiRouter.authenticationToken, forHTTPHeaderField: "Authorization")
        URLRequest.timeoutInterval = NSTimeInterval(10 * 1000)
        
        let encoding = Alamofire.ParameterEncoding.JSON
        
        return encoding.encode(URLRequest, parameters: result.parameters).0
    }
}
