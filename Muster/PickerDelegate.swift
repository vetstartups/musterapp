//
//  UIPickerDelegate.swift
//  Muster
//
//  Created by jhampac on 3/14/16.
//  Copyright © 2016 jhampac. All rights reserved.
//

import UIKit

class PickerDelegate: NSObject, UIPickerViewDataSource, UIPickerViewDelegate
{
    private let key: String
    private let stringArray: [String]
    
    init(info: (key: String, data: [String]))
    {
        self.key = info.key
        self.stringArray = info.data
        super.init()
    }
    
    func createDictionary(selectedRow: Int) -> [String: String]
    {
        let userPick = self.stringArray[selectedRow]
        let info = [self.key: userPick]
        return info
    }
    
    // MARK: - UIPicker DataSource
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        return self.stringArray.count
    }
    
    // MARK: - UIPicker Delegate Methods
    
    func pickerView(pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let string = stringArray[row]
        return NSAttributedString(string: string, attributes: [NSForegroundColorAttributeName: UIColor.whiteColor()])
    }
    
    func pickerView(pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat
    {
        return 44
    }
}
