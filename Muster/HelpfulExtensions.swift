//
//  HelpfulExtensions.swift
//  Muster
//
//  Created by jhampac on 3/15/16.
//  Copyright © 2016 jhampac. All rights reserved.
//

import UIKit

extension Dictionary
{
    mutating func update(other: Dictionary)
    {
        for (key, value) in other {
            self.updateValue(value, forKey: key)
        }
    }
}
